<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Admin
 */

Route::prefix('/admin')->group(function () {
    Route::post('/login/', 'Admin\\Auth\\LoginController@login');
});

Route::middleware('auth:api')->prefix('/admin')->group(function () {
    Route::post('/logout', 'Admin\\Auth\\LoginController@logout');
});

// Forms
Route::put('/admin/administration/form', 'API\Admin\Administration\FormsApiController@store');
Route::post('/admin/administration/form', 'API\Admin\Administration\FormsApiController@index');
Route::get('/admin//administration/form/{id}', 'API\Admin\Administration\FormsApiController@show');
Route::delete('/admin/administration/form/{id}', 'API\Admin\Administration\FormsApiController@destroy');

/**
 * Web
 */

Route::prefix('/web')->group(function () {
    Route::post('/login/', 'Web\\Auth\\LoginController@login');
});

Route::middleware('auth:api')->prefix('/web')->group(function () {
    Route::post('/logout', 'Web\\Auth\\LoginController@logout');
});
