<?php

namespace App\Http\Controllers\Api\Admin\Administration;

use App\Models\Form;
use App\Models\Question;
use App\Models\Answer;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFormRequest;
use App\Http\Requests\UpdateFormRequest;
use App\Http\Resources\Admin\FormResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;

class FormsApiController extends Controller
{
    public function index()
    {
        return new FormResource(Form::all());
    }

    public function store(StoreFormRequest $request)
    {
        $request['start_publish'] = $request->has('start_publish') ? date("Y-m-d", strtotime($request['start_publish'])) : '';
        $request['end_publish'] = $request->has('end_publish') ? date('Y-m-d', strtotime($request['end_publish'])) : '';
        $request['user_id'] = auth()->guard('api')->user()->id;
		
		// Add some backend validation. Check for name and only save if a authenticated user
		$this->validate($request, [
            'name' => 'required',
            'user_id'    => [
                'required',
                'integer',
            ],
        ]);
		
        $form = Form::create($request->all());		
		
		if (count($request->questions) >=1) {
			for($i = 0; $i<count($request->questions); $i++) {
                
                $dbQuestion = $form->questions()->create([
					'description' => $request->questions[$i]['description'],
					'type' => $request->questions[$i]['type'],
                    'mandatory' =>  $request->questions[$i]['mandatory']
                ]);
                $lastQ = $dbQuestion->id;

                // If there is questions, iterate through possible answers 
                if (count($request->questions[$i]['answers']) >=1) {                    
                    $arr['answers'] = $request->questions[$i]['answers'];
                    for($x = 0; $x<count($request->questions[$i]['answers']); $x++) {
                        Answer::create([
                            'valid_value' => $arr['answers'][$x]['valid_value'],
                            'question_id' => $lastQ
                        ]);
                    }
                }
				
			}
		}
		
		return (new FormResource($form))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Request $request, Form $form)
    {
        $showForm = Form::find($request->id);        
        $qs = DB::table('question')
        ->select('*')
        ->join('answer', 'answer.question_id', '=', 'question.id')
        ->where('question.form_id', '=', $request->id)
        ->get();
        $showForm['questions'] = $qs;
        return new FormResource($showForm);
    }

    public function update(UpdateFormRequest $request, Form $form)
    {
        $request['user_id'] = auth()->guard('api')->user()->id;
		
		// Add some backend validation. Check for name and only save if a authenticated user
		$this->validate($request, [
            'name' => 'required',
            'user_id'    => [
                'required',
                'integer',
            ],
        ]);
        $form->update($request->all());

        return (new FormResource($form))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Request $request, Form $form)
    {
        $delform = Form::findOrFail($request->id);
        $delform->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
