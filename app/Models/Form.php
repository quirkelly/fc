<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Illuminate\Database\Eloquent\Model;

class Form extends Model implements Auditable
{
    use AuditableTrait, SoftDeletes;

    public $table = 'form';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'introduction',
        'start_publish',
        'end_publish',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    public function author()
    {
        return $this->belongsTo('\App\Models\User');
    }

    public function questions()
    {
        return $this->hasMany('\App\Models\Question');
    }
}
