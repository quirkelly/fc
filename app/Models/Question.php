<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Illuminate\Database\Eloquent\Model;

class Question extends Model implements Auditable
{
    use AuditableTrait, SoftDeletes;

     public $table = 'question';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'type',
        'mandatory',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    public function form()
    {
        return $this->belongsTo('\App\Models\Form');
    }

    public function answer()
    {
        return $this->hasMany('\App\Models\Answer');
    }

    public function usersanswers()
    {
        return $this->hasMany('\App\Models\UserAnswer');
    }
}
