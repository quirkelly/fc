<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormTable extends Migration
{
    public function up()
    {
        Schema::create('form', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 256);
			$table->longText('description')->nullable(); 
			$table->longText('introduction')->nullable(); 
			$table->date('start_publish')->nullable();
            $table->date('end_publish')->nullable();
            $table->timestamps();

            $table->softDeletes();
        });
    }
}
