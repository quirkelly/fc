<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionTable extends Migration
{
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {
            $table->increments('id');
			$table->longText('description')->nullable(); 
            $table->enum('type', ['number', 'text', 'date', 'radio', 'dropdown'])->default('number');
            $table->boolean('mandatory')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
