<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswerTable extends Migration
{
    public function up()
    {
        Schema::create('answer', function (Blueprint $table) {
            $table->increments('id');
			$table->string('valid_value', 256);
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
