<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAnswerTable extends Migration
{
    public function up()
    {
        Schema::create('user_answer', function (Blueprint $table) {
            $table->increments('id');
			$table->longText('text_answer')->nullable(); 
			$table->date('date_answer')->nullable();
			$table->float('number_answer')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
