<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToQuestionTable extends Migration
{
    public function up()
    {
        Schema::table('question', function (Blueprint $table) {
            $table->unsignedInteger('form_id')->nullable();
            $table->foreign('form_id', 'form_fk_344254')->references('id')->on('form');
        });
    }
}
