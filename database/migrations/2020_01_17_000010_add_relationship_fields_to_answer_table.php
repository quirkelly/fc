<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToAnswerTable extends Migration
{
    public function up()
    {
        Schema::table('answer', function (Blueprint $table) {
            $table->unsignedInteger('question_id')->nullable();

            $table->foreign('question_id', 'question_fk_344254')->references('id')->on('question');
        });
    }
}
