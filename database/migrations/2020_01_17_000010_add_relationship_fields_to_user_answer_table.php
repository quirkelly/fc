<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToUserAnswerTable extends Migration
{
    public function up()
    {
        Schema::table('user_answer', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('question_id')->nullable();
            $table->unsignedInteger('answer_id')->nullable();
            $table->foreign('user_id', 'user_fk_344254')->references('id')->on('users');
			$table->foreign('question_id', 'question_fk_344254')->references('id')->on('question');
			$table->foreign('answer_id', 'answer_fk_344254')->references('id')->on('answer');
        });
    }
}
