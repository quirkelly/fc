-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.26 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table test.answer
DROP TABLE IF EXISTS `answer`;
CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `valid_value` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `question_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_fk_344254` (`question_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.answer: 6 rows
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` (`id`, `valid_value`, `created_at`, `updated_at`, `deleted_at`, `question_id`) VALUES
	(1, 'first answer', '2020-01-18 13:51:56', '2020-01-18 13:51:56', NULL, 2),
	(2, 'second answer', '2020-01-18 13:51:56', '2020-01-18 13:51:56', NULL, 2),
	(3, 'third answer', '2020-01-18 13:51:56', '2020-01-18 13:51:56', NULL, 2),
	(4, 'less than 20', '2020-01-18 13:51:56', '2020-01-18 13:51:56', NULL, 3),
	(5, '> 20 and <40', '2020-01-18 13:51:56', '2020-01-18 13:51:56', NULL, 3),
	(6, '40 or older', '2020-01-18 13:51:56', '2020-01-18 13:51:56', NULL, 3);
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;

-- Dumping structure for table test.audits
DROP TABLE IF EXISTS `audits`;
CREATE TABLE IF NOT EXISTS `audits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `event` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_id` bigint(20) unsigned NOT NULL,
  `old_values` text COLLATE utf8mb4_unicode_ci,
  `new_values` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(1023) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `audits_auditable_type_auditable_id_index` (`auditable_type`,`auditable_id`),
  KEY `audits_user_id_user_type_index` (`user_id`,`user_type`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.audits: 10 rows
/*!40000 ALTER TABLE `audits` DISABLE KEYS */;
INSERT INTO `audits` (`id`, `user_type`, `user_id`, `event`, `auditable_type`, `auditable_id`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `tags`, `created_at`, `updated_at`) VALUES
	(1, 'App\\Models\\User', 1, 'created', 'App\\Models\\Form', 1, '[]', '{"start_publish":"2020-01-31","end_publish":"2020-03-03","name":"Form 1","description":"Form 1 description","introduction":"form 1 intro","user_id":1,"id":1}', 'http://127.0.0.1:8000/api/admin/administration/form', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', NULL, '2020-01-18 13:51:56', '2020-01-18 13:51:56'),
	(2, 'App\\Models\\User', 1, 'created', 'App\\Models\\Question', 1, '[]', '{"description":"q1 for form 1","type":"text","mandatory":0,"form_id":1,"id":1}', 'http://127.0.0.1:8000/api/admin/administration/form', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', NULL, '2020-01-18 13:51:56', '2020-01-18 13:51:56'),
	(3, 'App\\Models\\User', 1, 'created', 'App\\Models\\Question', 2, '[]', '{"description":"question 2 for form 1","type":"radio","mandatory":0,"form_id":1,"id":2}', 'http://127.0.0.1:8000/api/admin/administration/form', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', NULL, '2020-01-18 13:51:56', '2020-01-18 13:51:56'),
	(4, 'App\\Models\\User', 1, 'created', 'App\\Models\\Answer', 1, '[]', '{"valid_value":"first answer","question_id":2,"id":1}', 'http://127.0.0.1:8000/api/admin/administration/form', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', NULL, '2020-01-18 13:51:56', '2020-01-18 13:51:56'),
	(5, 'App\\Models\\User', 1, 'created', 'App\\Models\\Answer', 2, '[]', '{"valid_value":"second answer","question_id":2,"id":2}', 'http://127.0.0.1:8000/api/admin/administration/form', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', NULL, '2020-01-18 13:51:56', '2020-01-18 13:51:56'),
	(6, 'App\\Models\\User', 1, 'created', 'App\\Models\\Answer', 3, '[]', '{"valid_value":"third answer","question_id":2,"id":3}', 'http://127.0.0.1:8000/api/admin/administration/form', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', NULL, '2020-01-18 13:51:56', '2020-01-18 13:51:56'),
	(7, 'App\\Models\\User', 1, 'created', 'App\\Models\\Question', 3, '[]', '{"description":"How old are you","type":"dropdown","mandatory":0,"form_id":1,"id":3}', 'http://127.0.0.1:8000/api/admin/administration/form', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', NULL, '2020-01-18 13:51:56', '2020-01-18 13:51:56'),
	(8, 'App\\Models\\User', 1, 'created', 'App\\Models\\Answer', 4, '[]', '{"valid_value":"less than 20","question_id":3,"id":4}', 'http://127.0.0.1:8000/api/admin/administration/form', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', NULL, '2020-01-18 13:51:56', '2020-01-18 13:51:56'),
	(9, 'App\\Models\\User', 1, 'created', 'App\\Models\\Answer', 5, '[]', '{"valid_value":"> 20 and <40","question_id":3,"id":5}', 'http://127.0.0.1:8000/api/admin/administration/form', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', NULL, '2020-01-18 13:51:56', '2020-01-18 13:51:56'),
	(10, 'App\\Models\\User', 1, 'created', 'App\\Models\\Answer', 6, '[]', '{"valid_value":"40 or older","question_id":3,"id":6}', 'http://127.0.0.1:8000/api/admin/administration/form', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', NULL, '2020-01-18 13:51:56', '2020-01-18 13:51:56');
/*!40000 ALTER TABLE `audits` ENABLE KEYS */;

-- Dumping structure for table test.form
DROP TABLE IF EXISTS `form`;
CREATE TABLE IF NOT EXISTS `form` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `introduction` longtext COLLATE utf8mb4_unicode_ci,
  `start_publish` date DEFAULT NULL,
  `end_publish` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_fk_344254` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.form: 1 rows
/*!40000 ALTER TABLE `form` DISABLE KEYS */;
INSERT INTO `form` (`id`, `name`, `description`, `introduction`, `start_publish`, `end_publish`, `created_at`, `updated_at`, `deleted_at`, `user_id`) VALUES
	(1, 'Form 1', 'Form 1 description', 'form 1 intro', '2020-01-31', '2020-03-03', '2020-01-18 13:51:56', '2020-01-18 13:51:56', NULL, 1);
/*!40000 ALTER TABLE `form` ENABLE KEYS */;

-- Dumping structure for table test.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.migrations: 18 rows
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2019_05_23_124336_add_is_admin_field_to_users_table', 1),
	(9, '2019_05_30_133248_seed_admin_user', 1),
	(10, '2020_01_16_161140_create_audits_table', 1),
	(11, '2020_01_17_000004_create_answer_table', 1),
	(12, '2020_01_17_000004_create_form_table', 1),
	(13, '2020_01_17_000004_create_question_table', 1),
	(14, '2020_01_17_000004_create_user_answer_table', 1),
	(15, '2020_01_17_000010_add_relationship_fields_to_answer_table', 1),
	(16, '2020_01_17_000010_add_relationship_fields_to_form_table', 1),
	(17, '2020_01_17_000010_add_relationship_fields_to_question_table', 1),
	(18, '2020_01_17_000010_add_relationship_fields_to_user_answer_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table test.oauth_access_tokens
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.oauth_access_tokens: 20 rows
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('efa85f55e48709fadeaa148506b831e6963b960ac1b637dd74497e76d1618bf22735aa9a40b34250', 1, 1, 'admin_user_impersonate', '["*"]', 0, '2020-01-18 04:59:50', '2020-01-18 04:59:50', '2021-01-18 04:59:50'),
	('32037c08378d976dd3b7f831bdc93c88b13739dadfddc8543e928d992e9b79ed7fd18d2d236fa57a', 2, 1, 'admin_user_impersonate', '["*"]', 0, '2020-01-18 04:59:57', '2020-01-18 04:59:57', '2021-01-18 04:59:57'),
	('b5aa45c9c191a93dbc01b882fe7d9639699ba7afde40a5f5bc37b573419eeaebaadff1eae45be149', 1, 1, 'admin_user_impersonate', '["*"]', 0, '2020-01-18 05:05:31', '2020-01-18 05:05:31', '2021-01-18 05:05:31'),
	('de960f48262d9bc89d1d31cba5f993d97d054dfa0522af0d3801032a390ad12b5d55dc2e17bd3366', 1, 1, 'admin_user_impersonate', '["*"]', 0, '2020-01-18 05:08:00', '2020-01-18 05:08:00', '2021-01-18 05:08:00'),
	('411aa1619f861f84263e920f25a878b0799c744d65d6d425bc342d64d1f1083aee79b0f3dae4d6e3', 1, 1, 'admin_user_impersonate', '["*"]', 1, '2020-01-18 05:37:56', '2020-01-18 05:37:56', '2021-01-18 05:37:56'),
	('7e23ad315a2747fba53a8c82a76be791cb3399e7aa9d7e2113d6b0d3cd893984e0ff3d45f4832823', 1, 1, 'admin_user_impersonate', '["*"]', 0, '2020-01-18 10:44:52', '2020-01-18 10:44:52', '2021-01-18 10:44:52'),
	('3407fa80744e48579108198e19baac8e17c121ff3acccf92731b880ec77052d872c230107fb47f2c', 1, 1, 'admin_user_impersonate', '["*"]', 0, '2020-01-18 11:33:24', '2020-01-18 11:33:24', '2021-01-18 11:33:24'),
	('5415619e2bd240db3fe4f2056a310648d9420045bc558152c99cf33aa8f86171c9090df437280e80', 1, 1, 'admin_user_impersonate', '["*"]', 0, '2020-01-18 12:01:22', '2020-01-18 12:01:22', '2021-01-18 12:01:22'),
	('e59691eb79d432b103eb970c9b9a9f4e4c03504e37fb540cf4ae606033f3f21be7a7b925614d259a', 2, 1, 'admin_user_impersonate', '["*"]', 1, '2020-01-18 12:11:09', '2020-01-18 12:11:09', '2021-01-18 12:11:09'),
	('2fbdb52f4734fc1546b864d15473c0725f8d943bc65a4420b03f4cdd37eb3b19156914e8c312b40e', 1, 1, 'admin_user_impersonate', '["*"]', 1, '2020-01-18 12:11:27', '2020-01-18 12:11:27', '2021-01-18 12:11:27'),
	('581f0c89d195af2b4271c8efbc05cd0a17f0379616eb1c879c7dbda35ed475a8e0bf5a804951077f', 1, 1, 'admin_user_impersonate', '["*"]', 1, '2020-01-18 12:11:47', '2020-01-18 12:11:47', '2021-01-18 12:11:47'),
	('99dd509b37cab183c04230f680aca99b29f2033bb61448e87f880fa6a7316d48ba561ed83ae18097', 2, 1, 'admin_user_impersonate', '["*"]', 1, '2020-01-18 12:14:29', '2020-01-18 12:14:29', '2021-01-18 12:14:29'),
	('1bd1df19fc8eaa35bf68dacfaa70730300844d0ba9a50e38b7b5dac4610278e25a44f14bb6596ead', 1, 1, 'admin_user_impersonate', '["*"]', 1, '2020-01-18 12:26:40', '2020-01-18 12:26:40', '2021-01-18 12:26:40'),
	('56ff597ad5e7995dc73bb27d39090fe59c71c04ff6d41fcd46e7dd2cc4d7765f07aa299a89dd8e4c', 1, 1, 'admin_user_impersonate', '["*"]', 1, '2020-01-18 12:59:41', '2020-01-18 12:59:41', '2021-01-18 12:59:41'),
	('ee2e2fde6580f2592f4f514ac70e47e7e0e0bb8928bcafcf034df3e7f9176d129f39d8e442cb97c2', 1, 1, 'admin_user_impersonate', '["*"]', 0, '2020-01-18 13:00:14', '2020-01-18 13:00:14', '2021-01-18 13:00:14'),
	('ec0adedfc2cd64f93410a9ef0d1da6255b8bcb5d4ada92aaedef4cae4c73fb4e118278365a02adf1', 1, 1, 'admin_user_impersonate', '["*"]', 1, '2020-01-18 13:17:32', '2020-01-18 13:17:32', '2021-01-18 13:17:32'),
	('ada70f8d1ac4f81667f46647070ed9627d445918ddceb7e0d4862b9e91e4bca5d87911596ce9f54a', 1, 1, 'admin_user_impersonate', '["*"]', 1, '2020-01-18 13:18:00', '2020-01-18 13:18:00', '2021-01-18 13:18:00'),
	('b0e5fda083e08595dc6977af0fded8b0ceeac1e52e5b9637faf9096e9f1754a26a37cf95d2bfd430', 2, 1, 'admin_user_impersonate', '["*"]', 1, '2020-01-18 13:24:44', '2020-01-18 13:24:44', '2021-01-18 13:24:44'),
	('65fc96fbea2940f05fb5624caf867a8cacd2d4eda8bc413f3ddb94e34c279cc65afc104434b2df0e', 1, 1, 'admin_user_impersonate', '["*"]', 0, '2020-01-18 13:41:10', '2020-01-18 13:41:10', '2021-01-18 13:41:10'),
	('ce2c07a6b736b80ea7bcc10ad0b318df9f35a757db4c28aa69d9faf27dceb916ab1f921cb1514734', 2, 1, 'admin_user_impersonate', '["*"]', 0, '2020-01-18 13:44:19', '2020-01-18 13:44:19', '2021-01-18 13:44:19');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table test.oauth_auth_codes
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.oauth_auth_codes: 0 rows
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table test.oauth_clients
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.oauth_clients: 2 rows
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Laravel Personal Access Client', '8XQgAnq7Xho5mHAEDnJHu52M8NgHAlduIc8ogCjF', 'http://localhost', 1, 0, 0, '2020-01-18 04:59:29', '2020-01-18 04:59:29'),
	(2, NULL, 'Laravel Password Grant Client', 'Fwn0KuwfngogcLbkZ2M25OzjdjFv7lUhvuC47svi', 'http://localhost', 0, 1, 0, '2020-01-18 04:59:29', '2020-01-18 04:59:29');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table test.oauth_personal_access_clients
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.oauth_personal_access_clients: 1 rows
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2020-01-18 04:59:29', '2020-01-18 04:59:29');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table test.oauth_refresh_tokens
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.oauth_refresh_tokens: 0 rows
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table test.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.password_resets: 0 rows
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table test.question
DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `type` enum('number','text','date','radio','dropdown') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'number',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `form_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `form_fk_344254` (`form_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.question: 3 rows
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` (`id`, `description`, `type`, `mandatory`, `created_at`, `updated_at`, `deleted_at`, `form_id`) VALUES
	(1, 'q1 for form 1', 'text', 0, '2020-01-18 13:51:56', '2020-01-18 13:51:56', NULL, 1),
	(2, 'question 2 for form 1', 'radio', 0, '2020-01-18 13:51:56', '2020-01-18 13:51:56', NULL, 1),
	(3, 'How old are you', 'dropdown', 0, '2020-01-18 13:51:56', '2020-01-18 13:51:56', NULL, 1);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;

-- Dumping structure for table test.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.users: 2 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `is_admin`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Administrator', 'admin@test.acme', '2020-01-18 04:56:01', '$2y$10$papqo0oHeH9MYITVHnMzx.V50mJiTwhqV5FNj71D6HA1lBjLheTpe', 1, NULL, '2020-01-18 04:56:01', '2020-01-18 04:56:01', NULL),
	(2, 'User', 'user@test.acme', '2020-01-18 04:56:01', '$2y$10$XjGxdC5X13yzvOiht4MDbe9Q/c0EyBQAvai4Eaj9UpNKDXuPNPhgy', 0, NULL, '2020-01-18 04:56:01', '2020-01-18 04:56:01', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table test.user_answer
DROP TABLE IF EXISTS `user_answer`;
CREATE TABLE IF NOT EXISTS `user_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text_answer` longtext COLLATE utf8mb4_unicode_ci,
  `date_answer` date DEFAULT NULL,
  `number_answer` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `question_id` int(10) unsigned DEFAULT NULL,
  `answer_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_fk_344254` (`user_id`),
  KEY `question_fk_344254` (`question_id`),
  KEY `answer_fk_344254` (`answer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table test.user_answer: 0 rows
/*!40000 ALTER TABLE `user_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_answer` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
